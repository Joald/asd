#include <iostream>
#include <vector>
#include <deque>
#include <functional>


using std::cout;
using std::cin;
using std::vector;
bool vis[500000];
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    int n, m;
    cin >> n >> m;
    vector<vector<int>> v(n);
    for (int i = 0; i < m; ++i) {
        int a, b;
        cin >> a >> b;
        --a; --b;
        v[a].push_back(b);
        v[b].push_back(a);
    }
    vector<int> depth(n);


    std::function<void(int, int)> dfs = [&](int u, int dp) {
        vis[u] = true;
        depth[u] = dp;
        for (auto& i : v[u]) {
            if (!vis[i]) {
                dfs(i, dp + 1);
            } else if ((depth[u] - depth[i]) % 2 == 0) {
                cout << "NIE";
                exit(0);
            }
        }
    };

    for (int i = 0; i < n; ++i) {
        if (!vis[i]) {
            dfs(i, 0);
        }
    }
    cout << "TAK";
    return 0;
}