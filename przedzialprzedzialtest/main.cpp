/*#include <iostream>
#include <memory>

class IntervalTree {
    std::unique_ptr<IntervalTree> left, right;
    IntervalTree *father;
    int value, override, range_begin, range_end, mid;
    bool fake;
public:
    IntervalTree(int begin, int end, IntervalTree *new_father)
            : fake(false), value(0), override(0), range_begin(begin), range_end(end),
              father(new_father), mid((begin + end) / 2) {
        if (!this->isLeaf()) {
            left = std::unique_ptr<IntervalTree>(new IntervalTree(begin, mid, this));
            right = std::unique_ptr<IntervalTree>(new IntervalTree(mid + 1, end, this));
        }
    }

    bool isLeaf() {
        return range_begin == range_end;
    }

    void print_tree() {

        std::cout << "Node " << range_begin << " to " << range_end << " contains " << value << " + " << override
                  << "\n";
        if (!this->isLeaf()) {
            left->print_tree();
            right->print_tree();
        }
    }

    void blame() {
        left->override += override;
        left->fake = true;
        right->override += override;
        right->fake = true;
        value += override;
        override = 0;
        fake = false;
    }


    void insert(int begin, int end, int delta) {
        if (begin == range_begin and end == range_end) {
            if (isLeaf()) {
                value += delta;
            } else {
                override += delta;
                fake = true;
            }
        } else {
            value += delta;
            if (begin > mid) {
                right->insert(begin, end, delta);
            } else if (end <= mid) {
                left->insert(begin, end, delta);
            } else {
                left->insert(begin, mid, delta);
                right->insert(mid + 1, end, delta);
            }
        }
    }

    int query(int begin, int end) {
        if (begin == range_begin and end == range_end) {
            return value + override;
        } else {
            blame();
            if (begin > mid) {
                return right->query(begin, end);
            } else if (end <= mid) {
                return left->query(begin, end);
            } else {
                return left->query(begin, mid) + right->query(mid + 1, end);
            }
        }
    }
};

int main() {
    IntervalTree tree(1, 10, nullptr);

    tree.insert(2, 6, 3);
    tree.print_tree();
    std::cout << "\n";
    tree.insert(4, 8, -3);
    tree.print_tree();
    std::cout << "\n";
    std::cout << "\n";
    std::cout << tree.query(4, 6);
    std::cout << "\n";
    tree.print_tree();
    return 0;
}

*/
