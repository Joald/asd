#include <iostream>
#include <vector>
#include <queue>

#define convert(x) ((x) = (x) > 0 ? (x) - 1 : (x))
#define validate(x) ((x) >= 0)
using namespace std;
int main() {
    unsigned int n, m;
    cin >> n;
    vector<vector<int>>v(n);

    for (int i = 0; i < n; ++i) {
        int a, b;
        cin >> a >> b;

        convert(a);
        convert(b);
        if (validate(a)) {
            v[i].push_back(a);
            v[a].push_back(i);
        }
        if (validate(b)) {
            v[i].push_back(b);
            v[b].push_back(i);
        }
    }
    cin >> m;
    vector<pair<int,int>> queries(m);
    for (auto& i : queries) {
        cin >> i.first >> i.second;

        convert(i.first);

    }
    vector<int> answers(m);
    for (auto& i : answers) {
        cin >> i;

        convert(i);

    }
    for (int i = 0; i < m; ++i) {
        queue<pair<int,int>> q;
        q.push({queries[i].first, 0});
        bool correct = false;
        vector<bool> check(n, false);
        check[queries[i].first] = true;
        while (!q.empty()) {
            auto curNode = q.front();
            if (curNode.first == answers[i] && curNode.second == queries[i].second) {
                correct = true;
                break;
            }
            q.pop();
            for (int j = 0; j < v[curNode.first].size(); ++j) {
                if (!check[v[curNode.first][j]]) {
                    check[v[curNode.first][j]] = true;
                    q.push({v[curNode.first][j], curNode.second + 1});
                }
            }
        }
        if (!correct and answers[i] != -1) {
            return i + 1;
        }
    }
    return 0;
}
