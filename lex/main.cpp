//#define debug
#include <vector>
#include <tuple>
#include <algorithm>
#ifdef debug
#include <fstream>

#define cin in
#define cout out
#else
#include <iostream>
#endif
using namespace std;
int flog(int n) {
    int i = 0;
    while (1 << i <= n) {
        ++i;
    }
    return i - 1;
}
#define generate
int main() {
#ifdef generate
    string a = "babababababababababababababababababababababababababa";
    int counter = 0;
    ofstream in("in.txt");
    ofstream out("out.txt");

    in << a.size() << " \n" << a << "\n";
    for (unsigned int i = 0; i < a.size(); ++i) {
        for (unsigned int j = i; j < a.size(); ++j) {
            for (unsigned int i2 = 0; i2 < a.size(); ++i2) {
                for (unsigned int j2 = i2; j2 < a.size(); ++j2) {
                    counter++;
                    in << i + 1 << " " << j + 1 << " " << i2 + 1 << " " << j2 + 1 << "\n";
                    string l = a.substr(i, j - i + 1);
                    string r = a.substr(i2, j2 - i2 + 1);
                    if (l > r) {
                        out << ">\n";
                    } else if (l < r) {
                        out << "<\n";
                    } else {
                        out << "=\n";
                    }
                }
            }
        }
    }
    cout << counter;
    return 0;
}
#else


    typedef std::tuple<int, int, int> triple;
#define first(x) std::get<0>(x)
#define second(x) std::get<1>(x)
#define third(x) std::get<2>(x)
#define f first(v[j])
#define s second(v[j])
#define t third(v[j])


#ifdef debug
    ifstream in("in.txt");
    ofstream out("progout.txt");

#endif
    unsigned long long n, m;
    in >> n >> m;
    //string a = "uwcjhlsmllwsgbuhpm";

    vector<vector<int>> tab(20, vector<int>(n));
    for (int i = 0; i < n; ++i) {
        char c;
        cin >> c;
        tab[0][i] = c;//a[i];
    }
    std::vector<triple> v(n);
    for (int i = 1; i < 20; ++i) {
        int j;
        for (j = 0; j < n - i - (i > 1); ++j) {
            f = tab[i - 1][j];
            s = tab[i - 1][j + 1 + (i > 1)];
            t = j;
        }
        for (; j < n; ++j) {
            f = std::numeric_limits<int>::max();
        }
        std::sort(v.begin(), v.end());
        int counter = 0;
        for (j = 0; j < n - i - (i > 1); ++j) {
            tab[i][t] = counter;
            if (f != first(v[j + 1]) or s != second(v[j + 1])) {
                counter++;
            }
        }
    }
    for (int j = 0; j < m; ++j) {
        int a, b, c, d;
        cin >> a >> b >> c >> d;
        a--;b--;c--;d--;
        int s1 = b + 1 - a, s2 = d + 1 - c;
        if (s1 > s2) {
            int l = flog(s2);
            if (tab[l][c] <= tab[l][a]) {
                cout << ">\n";
            } else {
                cout << "<\n";
            }
        } else if (s1 < s2) {
            int l = flog(s1);
            if (tab[l][a] <= tab[l][c]) {
                cout << "<\n";
            } else {
                cout << ">\n";
            }
        } else {
            int l = flog(s1);
            if (tab[l][a] < tab[l][c]) {
                cout << "<\n";
            } else if (tab[l][a] > tab[l][c]) {
                cout << ">\n";
            } else {
                int k1 = b - l;
                int k2 = d - l;
                if (tab[l][k1] < tab[l][k2]) {
                    cout << "<\n";
                } else if (tab[l][k1] > tab[l][k2]) {
                    cout << ">\n";
                } else {
                    cout << "=\n";
                }
            }
        }
    }
    return 0;
}                                                                                                                                                          

#endif