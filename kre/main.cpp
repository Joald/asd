#include <iostream>
#include <vector>

using namespace std;
typedef int v;
typedef int r;
const int mld = 2000000000;

#define mid midd(range_begin, range_end)
#define leaf isLeaf(range_begin, range_end)
#define base thisIsIt(range_begin, range_end, begin, end)
/*#define size sizef(range_begin, range_end)
#define lsize sizef(range_begin, mid)
#define rsize sizef(mid + 1, range_end)
*/
class IntervalTree {
    v maks, minn, override;

    IntervalTree *left, *right;

public:
    int midd(r begin, r end) {
        return (begin + end) / 2;
    }

    IntervalTree(r range_begin, r range_end, std::vector<int>::iterator &insert) : override(0) {
        if (!leaf) {
            left = new IntervalTree(range_begin, mid, insert);
            right = new IntervalTree(mid + 1, range_end, insert);
            minn = min(left->minn, right->minn);
            maks = max(left->maks, right->maks);
        } else {
            //number = ;
            maks = *insert;
            minn = *insert;
            insert++;
        }
    }

    int mini(r begin, r end, r range_begin, r range_end) {
        if (base) {
            return minn;
        }
        if (override != 0) {
            updateSons(range_begin, range_end);
        }
        if (begin > mid) {
            return right->mini(begin, end, mid + 1, range_end);
        } else if (end <= mid) {
            return left->mini(begin, end, range_begin, mid);
        } else {
            return min(left->mini(begin, mid, range_begin, mid), right->mini(mid + 1, end, mid + 1, range_end));
        }
    }

    int maxi(r begin, r end, r range_begin, r range_end) {
        if (base) {
            return maks;
        }
        if (override != 0) {
            updateSons(range_begin, range_end);
        }
        if (begin > mid) {
            return right->maxi(begin, end, mid + 1, range_end);
        }
        if (end <= mid) {
            return left->maxi(begin, end, range_begin, mid);
        } else {
            return max(left->maxi(begin, mid, range_begin, mid), right->maxi(mid + 1, end, mid + 1, range_end));
        }
    }

    bool isLeaf(r begin, r end) {
        return end == begin;
    }

    bool thisIsIt(r begin, r end, r range_begin, r range_end) {
        return begin == range_begin and end == range_end;
    }

    int sizef(r range_begin, r range_end) {
        return range_end - range_begin + 1;
    }

    void updateSons(r range_begin, r range_end) {
        left->override += override/* / size * lsize*/;
        right->override += override/* / size * rsize*/;
        left->minn += override/* / lsize*/;
        left->maks += override/* / lsize*/;
        right->minn += override/* / rsize*/;
        right->maks += override/* / rsize*/;
        override = 0;
        if (left->isLeaf(range_begin, mid)) {
            left->override = 0;
        }
        if (right->isLeaf(mid + 1, range_end)) {
            right->override = 0;
        }
    }

    void updateMinMax(r range_begin, r range_end) {
        minn = min(left->minn, right->minn);
        maks = max(left->maks, right->maks);
    }

    void set(r begin, r end, r range_begin, r range_end, v val) {
        if (base) {
            minn += val;
            maks += val;
            override += val /* * size */* !leaf;
        } else {
            if (override != 0) {
                updateSons(range_begin, range_end);
            }
            if (begin > mid) {
                right->set(begin, end, mid + 1, range_end, val);
            } else if (end <= mid) {
                left->set(begin, end, range_begin, mid, val);
            } else {
                //number = 0;
                left->set(begin, mid, range_begin, mid, val);
                right->set(mid + 1, end, mid + 1, range_end, val);
            }
            updateMinMax(range_begin, range_end);
        }

    }

    void print_tree(r range_begin, r range_end) {
        cout << "Node [" << range_begin << ".." << range_end << "] - min: " << minn << ", max: " << maks << ", override: " << override
             << "\n";
        if (!leaf) {
            left->print_tree(range_begin, mid);
            right->print_tree(mid + 1, range_end);
        }

    }
/*
 5 1 2 3 4 5 4 1 5 10 2 2 -100 3 4 2000000000 3 4 -12
 10 32 23 19 6 28 48 18 36 38 48 10 1 9 -31 8 10 -32 3 9 -23 4 10 -27 1 5 -5 3 7 -23 8 9 2 1 5 32 9 10 40 3 8 46

 test2
 100
 492 302 124 262 452 261 488 120 452 154 32 316 193 59 157 453 456 134 193 156 318 180 158 273 475 94 205 420 474 143 171 493 262 113 22 335 455 405 302 84 236 187 225 196 415 329 408 266 167 415 308 326 439 380 274 131 415 107 370 420 14 369 398 498 184 152 340 62 315 429 156 311 100 375 447 156 402 149 455 429 292 158 417 441 325 493 88 435 327 459 21 338 84 278 249 475 109 131 386 80
 200
 28 69 15
 57 100 -478
 21 70 376
 8 76 -302
 5 10 436
 13 32 -226
 11 65 -419
 21 89 -195
 54 54 -246
 77 95 294
 38 68 -104

 37 95 159
 27 86 -156
 40 81 -387
 59 84 -495
 56 79 90
 7 34 44
 18 94 383
 30 56 424
 83 86 237
 34 46 178
 //20
 9 89 -407
 23 29 4
 19 45 311
 2 91 -328
 1 37 425
 32 77 -285
 45 99 -353
 16 37 80
 48 55 157
 77 81 248
 //30
 16 98 20
 66 79 -369
 31 43 89
 66 68 493
 10 55 390
 18 93 -246
 19 60 -69
 28 38 -206
 59 78 -318
 18 77 -177

 34 82 -335
 3 88 289
 1 6 -472
 37 50 495
 14 94 -56
 34 95 -363
 26 93 -290
 4 81 -26
 36 63 347
 14 31 500
 40 81 -436

 32 41 -2
 3 7 439
 53 97 435
 3 13 -449
 80 94 -303
 39 80 -420
 44 92 19
 5 89 73
 18 83 105
 59 72 -212
 //60
 20 89 412
 74 76 22
 26
     */
};
const bool debug = false;
int main() {
    unsigned int n, m;
    cin >> n;
    vector<int> v1(n);
    for (auto &i : v1) {
        cin >> i;
    }
    cin >> m;
    auto iter = v1.begin();
    IntervalTree tree(1, n, iter);
    if (debug) tree.print_tree(1, n);
    int counter = 0;
    vector<int> v(n + 1);
    for (int i = 1; i < n; ++i) {
        v[i] = v1[i] - v1[i - 1];
        counter += v[i] > 0;
    }
    if (debug) {
        for (auto i : v) {
            cout << i << " ";
        }
        cout << "\n" << counter << "\n";
    }
    /// dla 1 < i < n
    /// v[i] = A[i - 1] - A[i - 2]
    for (int i = 0; i < m; ++i) {
        int a, b, delta;
        cin >> a >> b >> delta;
        int mini = tree.mini(a, b, 1, n);
        int maxi = tree.maxi(a, b, 1, n);
        if (mini + delta < 0 or maxi + delta > mld) {
            cout << -1 << "\n";
        } else {
            tree.set(a, b, 1, n, delta);
            if (a > 1) {
                counter += (v[a - 1] <= 0 and v[a - 1] + delta > 0) - (v[a - 1] > 0 and v[a - 1] + delta <= 0);
                v[a - 1] += delta;
            }
            if (b < n) {
                counter += (v[b] <= 0 and v[b] - delta > 0) - (v[b] > 0 and v[b] - delta <= 0);
                v[b] -= delta;
            }
            cout << counter << "\n";
        }
        if (debug) {
            cout << "Row " << i + 1 << "\n";
            for (auto j : v) {
                cout << j << " ";
            }
            cout << "\n";
            tree.print_tree(1, n);
        }
    }
    return 0;
}
