#include <iostream>
#include <vector>

using std::cin;
using std::cout;
const int BASE = 257;
const int MOD = 1000000007;

std::string word;
std::vector<long long> powers;
std::vector<long long> prefix_hashes;
#define mod(a, b) (((a) % (b) + (b)) % (b))
/*long long mod(long long a, long long b) {
    return (a % b + b) % b;
}*/
#define get_hash(i, j) ((i) == 0 ? prefix_hashes[j] : mod(prefix_hashes[j] - (prefix_hashes[(i) - 1] * powers[(j) - (i) + 1]), MOD))
/*long long get_hash(int i, int j) {
    return ;
}*/
#define are_equal(i, j, L) (get_hash((i), (i) + (L)) == get_hash((j), (j) + (L)))
/*bool are_equal(int i, int j, int L) {
    return ;
}*/
//#define is_suffix(a, b, c, d) ()
bool is_suffix(int a, int b, int c, int d) {
    return (b) - (a) <= (d) - (c) && are_equal((a), (c), (b) - (a));
}

int first_diff(int lo, int hi, int i, int j) {
    if (lo == hi) {
        return lo;
    }
    int mid = (lo + hi) / 2;
    return are_equal(i, j, mid) ? first_diff(mid + 1, hi, i, j) : first_diff(lo, mid, i, j);
}

char compare(int a, int b, int c, int d) {
    if (is_suffix(a, b, c, d)) {
        return b - a == d - c ? '=' : '<';
    }
    int f = first_diff(0, std::min(b - a, d - c), a, c);
    return word[a + f] < word[c + f] ? '<' : '>';
}


int main() {
    int N, M;
    cin >> N >> M;
    cin >> word;

    long long k = 1;
    long long s = 0;
    for (int i = 0; i < N; i++) {
        s += (word[i] - 'a' + 1);
        s *= BASE;
        s %= MOD;
        prefix_hashes.push_back(s);
        powers.push_back(k);
        k *= BASE;
        k %= MOD;
    }


    for (int i = 0; i < M; i++) {
        int a, b, c, d;
        cin >> a >> b >> c >> d;
        a--;b--;c--;d--;
        cout << compare(a, b, c, d) << "\n";
    }
    return 0;
}

/*#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>
#include <cmath>

using std::cin;
using std::cout;

void first() {
    cout << ">\n";
}

void second() {
    cout << "<\n";
}
void equal() {
    cout << "=\n";
}

void compare_and_print(int a, int b) {
    if (a < b) {
        second();
    } else if (a > b) {
        first();
    } else {
        equal();
    }
}

int main() {
    //freopen("in.txt", "r", stdin);
    unsigned int n, m;
    cin >> n >> m;
    std::vector<std::vector<int>> NAZWA(20, std::vector<int>(n));
    for (auto& i : NAZWA[0]) {
        char c;
        cin >> c;
        i = int(c - 'a') + 1;
    }
    for (int k = 0; k < NAZWA.size() - 1; ++k) {
        int two_to_k = 1 << k;
        std::vector<std::tuple<int, int, int>> tuples;
        for (int i = 0; i < n; ++i) {
            int left = NAZWA[k][i];
            int right = i + two_to_k >= n ? 'z' - 'a' + 2 : NAZWA[k][i + two_to_k];
            tuples.emplace_back(left, right, i);
        }
        std::sort(tuples.begin(), tuples.end());
        int kod = 1;
        for (int i = 0; i < n; ++i) {
            if (i && std::get<0>(tuples[i]) == std::get<0>(tuples[i - 1])
                && std::get<1>(tuples[i]) == std::get<1>(tuples[i - 1])) {
                kod--;
            }
            NAZWA[k + 1][std::get<2>(tuples[i])] = kod;
            kod++;
        }

    }
//    for (int i = 0; i < NAZWA.size(); ++i) {
//        cout << i << ": ";
//        for (auto& j : NAZWA[i]) {
//            cout << j << " ";
//        }
//        cout << "\n";
//    }
for (
int i = 0;
i<m;
++i) {
int a, b, c, d;
cin >> a >> b >> c >>
d;
a--;b--;c--;d--;
int len1 = b - a + 1;
int len2 = d - c + 1;
auto lg1 = static_cast<int>(std::log2(len1));
auto lg2 = static_cast<int>(std::log2(len2));
if (len1 ==
lg1 &&len2
==
lg2 &&lg1
== lg2) {
compare_and_print(NAZWA[lg1][a], NAZWA[lg2][c]
);
}
if (len1 > len2) {
int lbegin = NAZWA[lg2][a];
int rbegin = NAZWA[lg2][c];
if (lbegin > rbegin) {
first();

} else if (lbegin<rbegin) {
second();

} else {
int sndhalf = len2 - (1 << lg2);
int lend = NAZWA[lg2][a + sndhalf];
int rend = NAZWA[lg2][c + sndhalf];
if (lend<rend) {
second();

} else {
first();

}
}
} else if (len1<len2) {
int lbegin = NAZWA[lg1][a];
int rbegin = NAZWA[lg1][c];
if (lbegin > rbegin) {
first();

} else if (lbegin<rbegin) {
second();

} else {
int sndhalf = len1 - (1 << lg1);
int lend = NAZWA[lg1][a + sndhalf];
int rend = NAZWA[lg1][c + sndhalf];
if (lend > rend) {
first();

} else {
second();

}
}
} else {
int lbegin = NAZWA[lg1][a];
int rbegin = NAZWA[lg1][c];
if (lbegin > rbegin) {
first();

} else if (lbegin<rbegin) {
second();

} else {
int sndhalf = len1 - (1 << lg1);
int lend = NAZWA[lg1][a + sndhalf];
int rend = NAZWA[lg1][c + sndhalf];
if (lend<rend) {
second();

} else if (lend > rend){
first();

} else {
equal();

}
}
}
}
return 0;
}*/