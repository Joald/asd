#include <iostream>
#include <list>
#include <queue>
#include <limits>
#include <tuple>
#include <algorithm>

typedef long long ll;
typedef std::tuple<ll, ll, ll> tp;

class Graph {
    unsigned int size;
    unsigned int k;
    std::vector<std::list<tp>> adjacencies;
public:
    Graph(unsigned int size, unsigned int k) : size(size), k(k), adjacencies(size) {}

    void addEdge(int from, int to, int weight, int discountedWeight) {
        adjacencies[from].emplace_back(to, weight, discountedWeight);
    }

    ll findBOM() {
        std::priority_queue<tp, std::vector<tp>, std::greater<tp>> pq; // distance, coupons used, vertex
        std::vector<std::vector<ll>> dist(size, std::vector<ll>(k + 1, std::numeric_limits<ll>::max()));
        pq.emplace(0, 0, 0);
        for (auto &i : dist[0]) {
            i = 0;
        }
        while (!pq.empty()) {
            auto current = std::get<2>(pq.top());
            auto couponsUsed = std::get<1>(pq.top());
            pq.pop();
            for (auto &j : adjacencies[current]) {
                auto to = std::get<0>(j);
                auto weight = std::get<1>(j);
                auto discountedWeight = std::get<2>(j);

                if (dist[to][couponsUsed] > dist[current][couponsUsed] + weight) {
                    dist[to][couponsUsed] = dist[current][couponsUsed] + weight;
                    pq.emplace(dist[to][couponsUsed], couponsUsed, to);
                }
                if (couponsUsed < k && dist[to][couponsUsed + 1] > dist[current][couponsUsed] + discountedWeight) {
                    dist[to][couponsUsed + 1] = dist[current][couponsUsed] + discountedWeight;
                    pq.emplace(dist[to][couponsUsed + 1], couponsUsed + 1, to);
                }
            }
            if (current == size - 1) return *std::min_element(dist[current].begin(), dist[current].end());
        }
        return -1;
    }

};

int main() {
    unsigned int n, m, k;
    std::cin >> n >> m >> k;
    Graph g(n, k);
    for (int i = 0; i < m; ++i) {
        int v, w, b, c;
        std::cin >> v >> w >> b >> c;
        g.addEdge(v, w, c, c - b);
    }
    std::cout << g.findBOM();
    return 0;
}