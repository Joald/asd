#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

using std::cout;
using std::cin;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    unsigned int n, m;
    cin >> n >> m;
    std::vector<std::vector<std::vector<int>>> edges(n, std::vector<std::vector<int>>(2));
    using tr = std::pair<std::pair<int, int>, bool>;  std::priority_queue<tr, std::vector<tr>, std::greater<tr>> q;
    std::vector<int> paths(n, std::numeric_limits<int>::max());
    for (int i = 0; i < m; ++i) {
        int a, b, c;
        cin >> a >> b >> c;
        a--;b--;
        edges[a][c].push_back(b);
        edges[b][c].push_back(a);
    }
    q.emplace(std::make_pair(0, 0), true);
    q.emplace(std::make_pair(0, 0), false);
    std::vector<std::deque<bool>> vis(2, std::deque<bool>(n, false));
    vis[false][0] = true;
    vis[true][0] = true;
    while (!q.empty()) {
        auto a = q.top();
        q.pop();
        auto& makser = paths[a.first.second];
        makser = a.first.first < makser ? a.first.first : makser;
        for (auto& i : edges[a.first.second][!a.second]) {
            if (!vis[a.second][i]) {
                vis[a.second][i] = true;
                q.emplace(std::make_pair(a.first.first + 1, i), !a.second);
            }
        }
    }
    /*q.emplace(std::make_pair(0, 0), false);
    //std::for_each(vis.begin(), vis.end(), [](bool a){a = false;});
    while (!q.empty()) {
        auto a = q.top();
        q.pop();
        auto& makser = paths[a.first.second];
        makser = a.first.first < makser ? a.first.first : makser;
        for (auto& i : edges[a.first.second][!a.second]) {
            if (!vis[i]) {
                vis[i] = true;
                q.emplace(std::make_pair(a.first.first + 1, i), !a.second);
            }
        }
    }*/
    for (int j = 1; j < n; ++j) {
        cout << (paths[j] > m + 1 ? -1 : paths[j]) << "\n";
    }
    return 0;
}
/**
4 4
0 1 0
1 2 0
0 3 0
3 1 1
 *
 *
4 4
1 2 0
2 3 0
1 4 0
4 2 1
*/