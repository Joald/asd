#include <iostream>
#include <vector>
#include <queue>

using namespace std;

struct project {

};

int main() {
    unsigned int n, m, k;
    cin >> n >> m >> k;
    vector<int>costs(n);
    for (int i = 0; i < n; ++i) {
        cin >> costs[i];
    }
    vector<int> dependencies(n);
    vector<vector<int>> frees(n);
    for (int i = 0; i < m; ++i) {
        int a, b;
        cin >> a >> b;
        a--;
        b--;
        dependencies[a]++;
        frees[b].push_back(a);
    }
    auto cmp = [&](int l, int r){ return costs[l] > costs [r];};
    priority_queue<int, std::vector<int>, decltype(cmp)> q(cmp);
    for (int i = 0; i < n; ++i) {
        if (dependencies[i] == 0) {
            q.push(i);
        }
    }
    int count = 0;
    int cost = 0;
    while (count < k) {
        int current = q.top();
        q.pop();
        count++;
        cost = max(costs[current], cost);
        for (auto& i : frees[current]) {
            dependencies[i]--;
            if (!dependencies[i]) {
                q.push(i);
            }
        }

    }
    cout << cost;
    return 0;
}