#include <iostream>
#include <vector>
//30 mins
using namespace std;
typedef unsigned u;
typedef vector<u> t;

const int mod = 1000000000;

u left_son(u n) {
    return (n + 1) * 2 - 1;
}

u right_son(u n) {
    return (n + 1) * 2;
}

u father(u n) {
    if (n & 1) {
        return (n + 1) / 2 - 1;
    } else {
        return n / 2 - 1;
    }
}

u first_value(u tree_size, u n) {
    return tree_size - n;
}

void update_node(t &tree, int i) {
    tree[i] = tree[left_son(i)] + tree[right_son(i)];
}

void rebuild_tree(t &tree, u n) {
    for (int i = first_value(tree.size(), n) - 1; i >= 0; --i) {
        update_node(tree, i);
    }
}

void remove_elem(t &tree, u i, u n) {
    auto elem = first_value(tree.size(), n) + i - 1;
    tree[elem] = 0;
    while (elem != 0) {
        elem = father(elem);
        update_node(tree, elem);
    }
}

u query(t& tree, u left, u right, u n) {
    if (left > right) {
        return 0;
    }

    auto f = first_value(tree.size(), n);
    auto lval = f + left - 1, rval = f + right - 1;
    if (left == right) {
        return tree[lval];
    }
    auto lf = father(lval), rf = father(rval);
    if (lf == rf) {
        return tree[lf];
    }
    u sum = ((lval % 2 == 0 ? tree[lval]: 0) % mod + (rval % 2 == 1 ? tree[rval] : 0) % mod) % mod;
    while (lf != rf) {
        if (lval % 2 == 1) {
            sum = (sum + tree[lf]) % mod;
        }
        if (rval % 2 == 0) {
            sum = (sum + tree[rf]) % mod;
        }
        lval = lf;
        rval = rf;
        lf = father(lf);
        rf = father(rf);
    }
    return sum;
}

int main() {
    u n, k;
    cin >> n >> k;
    if (k > n) {
        cout << 0;
        return 0;
    }
    u power_of_two = 1;
    while (power_of_two < n) {
        power_of_two = power_of_two << 1;
    }
    auto tree_size = power_of_two * 2 - 1;
    u realn = n;
    n = power_of_two;
    t tree(tree_size, 1);
    t tab(n);
    auto first = first_value(tree_size, n);
    for (int i = first + realn; i < tree_size; ++i) {
        tree[i] = 0;
    }
    for (u i = 0; i < realn; ++i) {
        u a;
        cin >> a;
        tab[i] = a;
    }
    t buffer(n);
    for (u j = 0; j < k - 1; ++j) {
        rebuild_tree(tree, n);
        for (u i = 0; i < realn; ++i) {
            buffer[i] = query(tree, 1, tab[i] - 1, n);
            remove_elem(tree, tab[i], n);
        }
        for (u i = 0; i < realn; ++i) {
            tree[first + tab[i] - 1] = buffer[i];
        }
    }
    auto sum = 0;
    for (auto& i : buffer) {
        sum = (sum + i) % mod;
    }
    cout << sum;
    return 0;
}