#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <unordered_set>
#include <unordered_map>

using std::cout;
using std::cin;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    int n, l;
    cin >> n >> l;
    std::vector<std::unordered_set<int>> v(static_cast<unsigned long>(l));
    std::unordered_map<int, int> m;
    std::unordered_map<int, int> temp;

    long long maks = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < l; ++j) {
            int a;
            cin >> a;
            v[j].insert(a);
        }
    }
    for (auto& i : v) {
        for (auto &j : m) {
            if (i.find(j.first) != i.end()) {
                temp.insert(j);
            }
        }
        m.clear();
        m.swap(temp);
        for (auto& j : i) {
            auto& val = m[j];
            val++;
            maks = maks > val ? maks : val;
        }
    }
    cout << maks;
    return 0;
}