#include <iostream>
#include <unordered_set>

using std::cout;
using std::cin;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    std::unordered_set<int> s;
    int n, counter = 0;
    cin >> n;

    for (int i = 0; i < n; ++i) {
        int a;
        cin >> a;
        if (s.count(a)) {
            counter++;
            s.clear();
        } else {
            s.insert(a);
        }
    }
    cout << counter;
    return 0;
}