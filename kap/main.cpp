#include <iostream>
#include <queue>
#include <list>
#include <algorithm>
#include <cassert>

#define xc(n) v[n].first
#define yc(n) v[n].second
/*
struct p {
    std::pair<int, int> c;
};*/
typedef std::pair<int, int> p;

template<typename T>
bool min(T a, T b) {
    return a < b ? a : b;
}

template<typename T>
T modul(T a) {
    return a < 0 ? -a : a;
}

template<typename T>
bool applyIfBigger(T &a, T b) {
    if (b > a) return false;
    a = b;
    return true;
}

int main() {


    unsigned int n;
    std::cin >> n;
    std::vector<p> v;

    for (int i = 0; i < n; ++i) {
        int a, b;
        std::cin >> a >> b;
        v.emplace_back(a, b);
    }

    std::vector<bool> vis(n, false);

    std::vector<int> false_iksy;
    std::vector<int> false_igreki;
    for (int i = 0; i < n; ++i) {
        false_iksy.push_back(i);
        false_igreki.push_back(i);
    }
    std::sort(false_iksy.begin(), false_iksy.end(), [&](int left, int right) {
        return v[left].first == v[right].first ? v[left].second <= v[right].second : v[left].first < v[right].first;
    });
    std::sort(false_igreki.begin(), false_igreki.end(), [&](int left, int right) {
        return v[left].second == v[right].second ? v[left].first <= v[right].first : v[left].second < v[right].second;
    });

    std::vector<int> iksy(n);
    std::vector<int> igreki(n);
    for (int i = 0; i < n; ++i) {
        iksy[false_iksy[i]] = i;
        igreki[false_igreki[i]] = i;
    }

    std::vector<int> costs(n, std::numeric_limits<int>::max());
    //auto cmp = [&](int left, int right) {return costs[left] >= costs[right];};
    std::priority_queue<p, std::vector<p>, std::greater<p>> q;
    costs[0] = 0;
    q.push({0, 0});

    while (!q.empty() && !vis[n - 1]) {
        int current = q.top().second;
        q.pop();
        if (vis[current]) continue;
        vis[current] = true;
        int x = iksy[current];
        int y = igreki[current];
        assert(false_iksy[x] == current);
        assert(false_igreki[y] == current); // jak tego nie przejdzie to linie 39-40 są źle

        if (x < n - 1) {
            int neigh = false_iksy[x + 1];
            int newcost = costs[current] + modul(xc(neigh) - xc(current));
            if (applyIfBigger(costs[neigh], newcost) && !vis[neigh]) {
                q.push({newcost, neigh});
            }
        }
        if (x > 0) {
            int neigh = false_iksy[x - 1];
            int newcost = costs[current] + modul(xc(neigh) - xc(current));
            if (applyIfBigger(costs[neigh], newcost) && !vis[neigh]) {
                q.push({newcost, neigh});
            }
        }
        if (y < n - 1) {
            int neigh = false_igreki[y + 1];
            int newcost = costs[current] + modul(yc(neigh) - yc(current));
            if (applyIfBigger(costs[neigh], newcost) && !vis[neigh]) {
                q.push({newcost, neigh});
            }
        }
        if (y > 0) {
            int neigh = false_igreki[y - 1];
            int newcost = costs[current] + modul(yc(neigh) - yc(current));
            if (applyIfBigger(costs[neigh], newcost) && !vis[neigh]) {
                q.push({newcost, neigh});
            }
        }
        /*
        neigh = false_iksy[x + delta];
        prev_neigh = false_iksy[x + delta + 1];
        while (x + delta > 0 && (xc(neigh) == xc(current) || xc(prev_neigh) == xc(neigh))) {
            delta--;
            neigh = false_iksy[x + delta];
            prev_neigh = false_iksy[x + delta + 1];
            int newcost = costs[current] + modul(xc(neigh) - xc(current));
            if (applyIfBigger(costs[neigh], newcost) && !vis[neigh]) {
                q.push({newcost, neigh});
            }
        }
        delta = 0;
        neigh = false_igreki[y + delta];
        prev_neigh = false_igreki[y + delta - 1];
        while (y + delta < n - 1 && (yc(neigh) == yc(current) || yc(prev_neigh) == yc(neigh))) {
            delta++;
            neigh = false_igreki[y + delta];
            prev_neigh = false_igreki[y + delta - 1];
            int newcost = costs[current] + modul(yc(neigh) - yc(current));
            if (applyIfBigger(costs[neigh], newcost) && !vis[neigh]) {
                q.push({newcost, neigh});
            }
        }
        delta = 0;
        neigh = false_igreki[y + delta];
        prev_neigh = false_igreki[y + delta + 1];
        while (y + delta > 0 && (yc(neigh) == yc(current) || yc(prev_neigh) == yc(neigh))) {
            delta--;
            neigh = false_igreki[y + delta];
            prev_neigh = false_igreki[y + delta + 1];
            int newcost = costs[current] + modul(yc(neigh) - yc(current));
            if (applyIfBigger(costs[neigh], newcost) && !vis[neigh]) {
                q.push({newcost, neigh});
            }
        }*/
        if (current == n - 1) break;
    }
    std::cout << costs[n - 1];

    return 0;
}