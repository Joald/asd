#include <iostream>
#include <vector>
const int bilion = 1000000000;
///do końca zajęć

#define add(a,b) do{(a) = ((a) + (b)) % bilion;}while(false)

class Int {
    int val;
    Int():val(0){}
    Int&operator+=(const Int&rhs) {

    }
};

int main() {
    unsigned int n;
    std::cin >> n;
    if (n == 1) {
        std::cout << 1;
        return 0;
    }
    std::vector<int> tab(n);
    for (auto& i : tab) {
        std::cin >> i;
    }

    std::vector<int>prev_left(n);
    std::vector<int>prev_right(n);
    std::vector<int>cur_left(n);
    std::vector<int>cur_right(n);

    for (int i = 1; i < n; ++i) {
        prev_right[i] = tab[i] > tab[i - 1];
    }
    for (int i = 0; i < n - 1; ++i) {
        prev_left[i] = tab[i] < tab[i + 1];
    }
    for (int c = 3; c <= n; ++c) {
        int start = c - 1;
        for (int i = start; i < n; ++i) {
            if (tab[i] > tab[i - 1]) {
                add(cur_right[i], prev_right[i - 1]);
            }
            if (tab[i] > tab[i - start]) {
                add(cur_right[i], prev_left[i - start]);
            }
        }

        for (int i = 0; i < n - start; ++i) {
            if (tab[i] < tab[i + 1]) {
                add(cur_left[i], prev_left[i + 1]);
            }
            if (tab[i] < tab[i + start]) {
                add(cur_left[i], prev_right[i + start]);
            }
        }

        prev_left = std::move(cur_left);
        prev_right = std::move(cur_right);

        cur_left = std::vector<int>(n);
        cur_right = std::vector<int>(n);
    }

    std::cout << (*prev_left.begin() + *prev_right.rbegin()) % bilion;
    return 0;
}