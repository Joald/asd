%:include<iostream>
%:include<vector>
using namespace std;


struct tree {
	int v;
	tree* father;
	void join(tree* other) {
		if (other->rep() > this->rep()) {
			father = other;
		} else {
			other->father = this;
		}
	}
	tree* find() {
		tree* t = this;
		while(t->father) {
			t = t->father;
		}
		tree* pop = this;
		while(pop->father) {
			tree* temp = pop;
			pop = pop->father;
			temp->father = t;
		}
		return t;
	}
	int rep() {
		return find()->v;
	}
};


struct stack {
	tree* top = nullptr;
	stack* next = nullptr;
	int size = 0;
	void join_all_greater(tree* t) {
		if (!top) {
			size++;
			top = t;
		} else if (top->rep() > t->v) {
			top->join(t);
			stack* s = this;
			while(s->next) {
				s = s->next;
				top->join(s->top);
				size--;
			}
			next = nullptr;
			top = s->top;
		} else if (next) {
			next->join_all_greater(t);
			size = next->size + 1;
		} else {
			size++;
			next = new stack;
			next->top = t;
			next->size = 1;
		}
	}
};

int main() {
	int n;
	cin >> n;
	std::vector<tree> v(n);
	for (auto& i : v) {
		cin >> i.v;
		i.father = nullptr;
	}
	stack s;
	for (auto& i : v) {
		s.join_all_greater(&i);
	}
	cout << s.size << "\n";
	std::vector<std::vector<int>> ns(n); // we'll see if it works
	for (int i = 0; i < n; ++i) {
		ns[v[i].rep() - 1].push_back(i + 1);
	}
	for (auto& i : ns) {
		if (!i.empty()) {
			cout << i.size() << " ";
			for (auto& j : i) {
				cout << j << " ";
			}
			cout << "\n";
		}
	}
	return 0;
}

int find_union[1000008];

void MakeSet(int n) {
	for (int i = 0; i < n; ++i) {
		find_union[i] = i;
	}
}

int Find(int x) {
	if (find_union[x] != x) {
		find_union[x] = Find(find_union[x]);
	}
	return find_union[x];
}

void Union(int x, int y) {
	xParent = Find(x);
	yParent = Find(y);
	if (x > y) {
		find_union[y] = x;
	} else {
		find_union[x] = y;
	}
}

int main() {
	int n;
	cin >> n;
	MakeSet(n);
	std::vector<int> permutation(n);
	std::vector<int> stacc(n, -1);
	for (int i = 0; i < n; ++i) {
		cin >> permutation[i].first;
		permutation[i]--;
		for (int j = 0; j < n; ++j) {
			if (stacc[j] == -1) {
				stacc[j] = permutation[i];
				break;
			} else if (stacc[j] > permutation[i]) {
				int k = j;
				while (stacc[k] != -1) {
					k++;
				}
				best = stacc[k - 1];
				for (int l = j; l < k; ++l) {
					Union(stacc[l]], best);
					stacc[l] = -1;
				}
				Union(permutation[i], best);
				stacc[j] = best;
			}
		}
	}
	for (int i = 0; i < n; ++i) {
		
	}
}
