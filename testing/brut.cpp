#include <iostream>
#include <vector>
#define mld 2000000000
using namespace std;
int main() {
    unsigned int n, m;
    cin >> n;
    vector<int>v(n);
    for (auto& i : v) {
        cin >> i;
    }
    cin >> m;
    for (int i = 0; i < m; ++i) {
        int l, r, delta;
        cin >> l >> r >> delta;
        bool found = false;
        for (int j = l - 1; j <= r - 1; ++j) {
            if (v[j] + delta < 0 or v[j] + delta > mld) {
                found = true;
                break;
            }
        }
        if (found) {
            cout << -1 << "\n";
            continue;
        }
        for (int j = l - 1; j <= r - 1; ++j) {
            v[j] += delta;
        }
        int counter = 0;
        for (int j = 1; j < n; ++j) {
            counter += v[j] > v[j - 1];
        }
        cout << counter << "\n";
    }
}
