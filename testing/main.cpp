//#define BRUT

#ifndef BRUT
#include <iostream>
#include <vector>
#include <functional>

using namespace std;

#define convert(x) ((x) = (x) > 0 ? (x) - 1 : (x))

#define validate(x) ((x) >= 0)

#define mright (dir[a] == RIGHT)
#define mup (dir[a] == UP)

enum direction {
    LEFT, RIGHT, UP
};
int main() {
    unsigned int n;
    cin>>n;
    vector<pair<int, int>> sons(n);

    vector<vector<int>> ancestors(n, vector<int>(20, -1));
    vector<int> maxLeft(n);
    vector<int> distLeft(n);
    vector<direction> dir(n);
    vector<int> maxRight(n);
    vector<int> distRight(n);
    vector<int> maxUp(n, -1);
    vector<int> distUp(n);

    vector<int> depth(n);


    for (auto& i : sons) {
        cin >> i.first;
        convert(i.first);
        cin >> i.second;
        convert(i.second);
    }

    std::function<void(int)> process = [&](int currentNode) {
        int left = sons[currentNode].first;
        int right = sons[currentNode].second;

        if (validate(left)) {
            ancestors[left][0] = currentNode;
            depth[left] = depth[currentNode] + 1;
            process(left);
            if (validate(maxLeft[left]) && validate(maxRight[left])) {
                if (distLeft[left] > distRight[left]) {
                    maxLeft[currentNode] = maxLeft[left];
                    distLeft[currentNode] = distLeft[left] + 1;
                } else {
                    maxLeft[currentNode] = maxRight[left];
                    distLeft[currentNode] = distRight[left] + 1;
                }
            } else if (validate(maxLeft[left])) {
                maxLeft[currentNode] = maxLeft[left];
                distLeft[currentNode] = distLeft[left] + 1;
            } else if (validate(maxRight[left])) {
                maxLeft[currentNode] = maxRight[left];
                distLeft[currentNode] = distRight[left] + 1;
            } else {
                maxLeft[currentNode] = left;
                distLeft[currentNode] = 1;
            }
        } else {
            maxLeft[currentNode] = -1;
            distLeft[currentNode] = 0;
        }
        if (validate(right)) {
            ancestors[right][0] = currentNode;
            depth[right] = depth[currentNode] + 1;
            process(right);
            if (validate(maxLeft[right]) && validate(maxRight[right])) {
                if (distLeft[right] > distRight[right]) {
                    maxRight[currentNode] = maxLeft[right];
                    distRight[currentNode] = distLeft[right] + 1;
                } else {
                    maxRight[currentNode] = maxRight[right];
                    distRight[currentNode] = distRight[right] + 1;
                }
            } else if (validate(maxLeft[right])) {
                maxRight[currentNode] = maxLeft[right];
                distRight[currentNode] = distLeft[right] + 1;
            } else if (validate(maxRight[right])) {
                maxRight[currentNode] = maxRight[right];
                distRight[currentNode] = distRight[right] + 1;
            } else {
                maxRight[currentNode] = right;
                distRight[currentNode] = 1;
            }
        } else {
            maxRight[currentNode] = -1;
            distRight[currentNode] = 0;
        }
    };
    process(0);

    std::function<void(int)> setUpDists = [&](int currentNode) {
        auto father = ancestors[currentNode][0];
        if (validate(father)) {
            if (sons[father].first == currentNode) { //we are the left son
                if (distUp[father] >= distRight[father]) {
                    distUp[currentNode] = distUp[father] + 1;
                    maxUp[currentNode] = maxUp[father];
                } else {
                    distUp[currentNode] = distRight[father] + 1;
                    maxUp[currentNode] = maxRight[father];
                }
            } else {
                if (distUp[father] >= distLeft[father]) {
                    distUp[currentNode] = distUp[father] + 1;
                    maxUp[currentNode] = maxUp[father];
                } else {
                    distUp[currentNode] = distLeft[father] + 1;
                    maxUp[currentNode] = maxLeft[father];
                }
            }

        }
        if (distRight[currentNode] >= distLeft[currentNode] and distRight[currentNode] >= distUp[currentNode]) {
            dir[currentNode] = RIGHT;
        }
        if (distLeft[currentNode] >= distRight[currentNode] and distLeft[currentNode] >= distUp[currentNode]) {
            dir[currentNode] = LEFT;
        }
        if (distUp[currentNode] >= distLeft[currentNode] and distRight[currentNode] <= distUp[currentNode]) {
            dir[currentNode] = UP;
        }
        if (validate(sons[currentNode].first)) {
            setUpDists(sons[currentNode].first);
        }
        if (validate(sons[currentNode].second)) {
            setUpDists(sons[currentNode].second);
        }
    };
    setUpDists(0);

    std::function<void(int)> setAncestors = [&](int currentNode) {
        long long power = 0;
        while (validate(ancestors[currentNode][power]) && validate(ancestors[ancestors[currentNode][power]][power])) {
            ancestors[currentNode][power + 1] = ancestors[ancestors[currentNode][power]][power];
            power++;
        }
        if (validate(sons[currentNode].first)) {
            setAncestors(sons[currentNode].first);
        }
        if (validate(sons[currentNode].second)) {
            setAncestors(sons[currentNode].second);
        }
    };
    setAncestors(0);
    int m;
    cin >> m;
    for (int i = 0; i < m; ++i) {
        int a, d;
        cin >> a >> d;
        a--;
        if (d == 0) {
            cout << a + 1 << '\n';
            continue;
        }
        if (d > distLeft[a] && d > distRight[a] && d > distUp[a]) {
            cout << -1 << '\n';
            continue;
        }
        if (d == distLeft[a]) {
            cout << maxLeft[a] + 1 << '\n';
            continue;
        }
        if (d == distRight[a]) {
            cout << maxRight[a] + 1 << '\n';
            continue;
        }
        if (d == distUp[a]) {
            cout << maxUp[a] + 1 << '\n';
            continue;
        }
        if (mup) {
            int other = maxUp[a];
            if (other == 0) {
                int curDist = d;
                while (curDist) {
                    int power = 0;
                    while (1 << power <= curDist) {
                        power++;
                    }
                    if (1 << power > curDist) {
                        power--;
                    }
                    curDist -= 1 << power;
                    other = ancestors[other][power];
                }
                cout << other + 1 << '\n';
                continue;
            }
            int current = a;

            while (maxUp[current] == other) {
                int power = 0;
                while (maxUp[ancestors[current][power]] == other) {
                    power++;
                }
                current = ancestors[current][power <= 1 ? power : power - 1];
            }
            if (depth[a] - depth[current] == d) {
                cout << current + 1 << '\n';
            } else if (depth[a] - depth[current] > d) {
                int cur = a;
                int curDist = d;
                while (curDist) {
                    int power = 0;
                    while (1 << power <= curDist) {
                        power++;
                    }
                    if (1 << power > curDist) {
                        power--;
                    }
                    curDist -= 1 << power;
                    cur = ancestors[cur][power];
                }
                cout << cur + 1 << '\n';
            } else {
                int cur = other;
                int curDist = distUp[a] - d;
                while (curDist) {
                    int power = 0;
                    while (1 << power <= curDist) {
                        power++;
                    }
                    if (1 << power > curDist) {
                        power--;
                    }
                    curDist -= 1 << power;
                    cur = ancestors[cur][power];
                }
                cout << cur + 1 << '\n';
            }
            continue;
        }
        int distance;
        int other;
        if (mright) {
            distance = distRight[a];
            other = maxRight[a];
        } else {
            distance = distLeft[a];
            other = maxLeft[a];
        }
        int curDist = distance - d;
        while (curDist) {
            int power = 0;
            while (validate(ancestors[other][power + 1]) && 1 << (power + 1) <= curDist) {
                power++;
            }
            curDist -= 1 << power;
            other = ancestors[other][power];
        }
        cout << other + 1 << '\n';
    }
    return 0;
}
#else
#include <iostream>
#include <vector>
#include <queue>

#define convert(x) ((x) = (x) > 0 ? (x) - 1 : (x))

using namespace std;
int main() {
    unsigned int n, m;
    cin >> n;
    vector<vector<int>>v(n);

    for (int i = 0; i < n; ++i) {
        int a, b;
        cin >> a >> b;

        convert(a);
        convert(b);
        v[i].push_back(a);
        v[a].push_back(i);
        v[i].push_back(b);
        v[b].push_back(i);
    }
    cin >> m;
    vector<pair<int,int>> queries(m);
    for (auto& i : queries) {
        cin >> i.first >> i.second;

        convert(i.first);

    }
    vector<int> answers(m);
    for (auto& i : answers) {
        cin >> i;

        convert(i);

    }
    for (int i = 0; i < m; ++i) {
        queue<pair<int,int>> q;
        q.push({queries[i].first, 0});
        bool correct = false;
        vector<bool> check(n, false);
        check[queries[i].first] = true;
        while (!q.empty()) {
            auto curNode = q.front();
            if (curNode.first == answers[i] && curNode.second == queries[i].second) {
                correct = true;
                break;
            }
            q.pop();
            for (int j = 0; j < v[curNode.first].size(); ++j) {
                if (!check[v[curNode.first][j]]) {
                    check[v[curNode.first][j]] = true;
                    q.push({v[curNode.first][j], curNode.second + 1});
                }
            }
        }
        if (!correct) {
            return 1;
        }
    }
    return 0;
}

#endif