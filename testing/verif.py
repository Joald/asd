from subprocess import call
from random import randint, choice

	

testsize = 10
ranga = 100
call("./ver_cmp.sh", shell = True)
while(True):
	f = open('test.in', 'w')
	f.write(str(testsize) + '\n')
	tree = [[-1, -1]]
	free_edges = [(0, 0), (0, 1)]

	n = testsize  # how many nodes do you want?

	while len(tree) < n:
		e = choice(free_edges)  # select a free edge
		node, child = e
		assert tree[node][child] == -1  # make sure we made no mistake

		k = len(tree)  # index of new node
		tree.append([-1, -1])  # add new node

		tree[node][child] = k  # set new node as child of an old node
		free_edges.extend([(k, 0), (k, 1)])  # new node has two free edges

		free_edges.remove(e)  # edge is no longer free
	for i in tree:
		f.write(str(i[0] + 1 if not i[0] == -1 else -1) + ' ' + str(i[1] + 1 if not i[1] == -1 else -1) + '\n')
	f.write(str(ranga) + '\n')
	for i in range(ranga):
		a = randint(1, testsize)
		d = randint(0, testsize)
		f.write(str(a) + ' ' + str(d) + '\n')
	if (call("./verify.sh", shell=True) != 0):
		break
		
		
