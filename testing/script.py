from subprocess import call
from random import randint
testsize = 100000
ranga = 1000000
call("./cmp.sh", shell = True)
while(True):
	f = open('test.in', 'w')
	f.write(str(testsize) + '\n')
	for i in range(testsize):
		f.write(str(randint(1, ranga)) + ' ')
	f.write('\n' + str(testsize) + '\n')
	for i in range(testsize):
		l = randint(1, testsize - 1)
		r = randint(l + 1, testsize)
		delta = randint(-2 * ranga, 2 * ranga)
		f.write(str(l) + ' ' + str(r) + ' ' + str(delta) + '\n')
	if (call("./diffit.sh", shell=True) == 1):
		break;
