#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::cin;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    int n;
    cin >> n;
    std::vector<std::pair<std::pair<int, int>, bool>> v;
    for (int i = 0; i < n; ++i) {
        int a, b, c;
        cin >> a >> b >> c;
        v.emplace_back(std::make_pair(a, b), true);
        v.emplace_back(std::make_pair(a, c), false);
    }
    std::sort(v.begin(), v.end());
    long long counter = 0, sum = 0;

    for (auto& i : v) {
        if (i.second) {
            sum += counter++;
        } else {
            counter--;
        }
    }
    cout << sum;
    return 0;
}