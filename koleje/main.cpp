/*#include <iostream>

using namespace std;
typedef int v;
typedef int r;
#define mid midd(range_begin, range_end)
#define leaf isLeaf(range_begin, range_end)
#define base thisIsIt(range_begin, range_end, begin, end)

class IntervalTree {
    v sum, m;
    IntervalTree *left, *right;
public:
    int midd(r begin, r end) {
        return (begin + end) / 2;
    }

    v max(v l, v r) {
        return l > r ? l : r;
    }


    IntervalTree(r range_begin, r range_end) : sum(0), m(0) {
        if (!leaf) {
            left = new IntervalTree(range_begin, mid);
            right = new IntervalTree(mid + 1, range_end);
        }
    }

    bool isLeaf(r begin, r end) {
        return end == begin;
    }

    bool thisIsIt(r begin, r end, r range_begin, r range_end) {
        return begin == range_begin and end == range_end;
    }

    v maks(r begin, r end, r range_begin, r range_end) {
        if (base) {
            return m + sum;
        }
        if (begin > mid) {
            return max(sum, right->maks(begin, end, mid + 1, range_end));
        }
        if (end <= mid) {
            return max(sum, left->maks(begin, end, range_begin, mid));
        }
        return max(sum, max(left->maks(begin, mid, range_begin, mid), right->maks(mid + 1, end, mid + 1, range_end)));
    }

    void add(r begin, r end, r range_begin, r range_end, v val) {
        if (base) {
            sum = val + sum;
        } else {
            m = val + m;
            if (begin > mid) {
                right->add(begin, end, mid + 1, range_end, val);
            } else if (end <= mid) {
                left->add(begin, end, range_begin, mid, val);
            } else {
                left->add(begin, mid, range_begin, mid, val);
                right->add(mid + 1, end, mid + 1, range_end, val);
            }
        }
    }

    void print_tree(r range_begin, r range_end) {
        cout << "Node [" << range_begin << ".." << range_end << "] - sum: " << sum << ", m: " << m << "\n";
        if (!leaf) {
            left->print_tree(range_begin, mid);
            right->print_tree(mid + 1, range_end);
        }

    }
};

int main() {
    int n, m, z;
    cin >> n >> m >> z;
    IntervalTree tree(1, n);
    for (int i = 0; i < n; ++i) {
        int p, k, l;
        cin >> p >> k >> l;
        auto h = tree.maks(p, k, 1, n);
        //cout << "max on [" << x << ".." << x + l - 1 << "] is " << h << "\n";
        if (h + l <= m) {
            cout << "T\n";
            tree.add(p, k, 1, n, l);
        } else {
            cout << "N\n";
        }

        tree.print_tree(1, n);
    }
    return 0;
}*/

// Przeanalizuj poniższy kod. Odpowiedz na pytania.
// W jakich sytuacjach wywoływane są poszczególne konstruktory?
// Kiedy wywoływane są operatory przypisania i konwersji?
// W jakiej kolejności wywoływane są konstruktory obiektów składowych klasy?
// W jakiej kolejności wywoływane są destruktory?
// Kiedy konstruowane są wartości tymczasowe?
// Czym rożni się przekazywanie argumentów przez wartość i przez referencję?
// Jak funkcja zwraca wartość będącą obiektem?
// Co sie dzieje, gdy stała jest przekazywana przez const&?
// Dlaczego operator konwersji jest const, a operator przypisania nie?
// Dlaczego operator przypisania zwraca wartość typu Integer&, a nie const Integer&?
// Dlaczego funkcja operator* zwraca wartość typu const Rational, a nie Rational?

#include <iostream>
#include <typeinfo>

using namespace std;

template<class T>
class Integer {
public:
    Integer() : internal(0) {
        cout << *this << "::konstruktor_bezparametrowy()" << endl;
    }

    Integer(const Integer<T> &that) : internal(that.internal) {
        cout << *this << "::konstruktor_kopiujacy(" << that << ")" << endl;
    }

    Integer(Integer<T> &&that) : internal(move(that.internal)) {
        cout << *this << "::konstruktor_przenoszacy(" << that << ")" << endl;
    }

    // Zobacz, co sie stanie, gdy ponizszy konstruktor zostanie zadeklarowany tak:
    // explicit Integer(T i) : internal(i) {
    Integer(T i) : internal(i) {
        cout << *this << "::konstruktor_konwertujacy(" << i << ")" << endl;
    }

    ~Integer() {
        cout << *this << "::destruktor()" << endl;
    }

    Integer &operator=(const Integer &that) {
        cout << *this << "::operator_kopiujacy=(" << that << ")" << endl;
        internal = that.internal;
        return *this;
    }

    Integer &operator=(Integer &&that) {
        cout << *this << "::operator_przenoszacy=(" << that << ")" << endl;
        internal = move(that.internal);
        return *this;
    }

    // Zastanów się, co sie zmienia, gdy ponizszy operator zostanie zadeklarowany tak:
    // explicit operator int() const {
    operator int() const {
        cout << *this << "::operator int()" << endl;
        return internal;
    }

    Integer &operator*=(const Integer &that) {
        internal *= that.internal;
        return *this;
    }

private:
    T internal;

    template<class T1>
    friend ostream &operator<<(ostream &, const Integer<T1> &);
};

template<class T>
ostream &operator<<(ostream &os, const Integer<T> &i) {
    os << "{"
       << typeid(i).name()
       << "(" << i.internal
       << ")@"
       << static_cast<const void *>(&i) << "}";
    return os;
}

typedef Integer<int> Int;
typedef Integer<unsigned> Unsigned;

class Rational {
public:
    Rational(Int n, Unsigned d) : numerator(n), denominator(d) {}

    Rational &operator*=(const Rational &that) {
        // To jest badziewie, trzeba skracac przez NWD.
        numerator *= that.numerator;
        denominator *= that.denominator;
        return *this;
    }

private:
    Int numerator;
    Unsigned denominator;
};

const Rational operator*(const Rational &a, const Rational &b) {
    return Rational(a) *= b;
}

Int foo(Int i) {
    return i;
}

void foo(const Int &i, const Int &j) {
    cout << "wywolano foo(" << i << ", " << j << ")" << endl;
}

Int foo(void) {
    return Int();
}

int main() {
    {
        cout << "\nWykonuje sie konstruktor bezparametrowy i kopiujący." << endl;
        Int i, j(i);
        cout << "\nPrzy opuszczaniu zasiegu wykonuja sie destruktory." << endl;
    }
    {
        cout << "\nWykonuje sie konwersja typu." << endl;
        Int i('3');
        cout << "\nTu tez wykonuje sie konwersja typu." << endl;
        Int j = 4;
        cout << "\nA tu wykonuje sie operator przypisania." << endl;
        i = j;
        cout << "\nWykonuje sie ponowna konwersja typu." << endl;
        int k = j;
        cout << "\nWykonuje sie jeszcze jedna konwersja typu." << endl;
        char c = j;// jak operator int będzie explicit to compile error
        cout << "\nPrzekazywanie wartosci wywoluje konstruktor kopiujacy lub "
                "przenoszący (i potem destruktor)." << endl;
        foo(j);
        cout << "\nPrzekazywanie argumentów przez referencje." << endl;
        foo(i, j);
        cout << "\nZwracanie wartosci przez funkcje." << endl;
        i = move(foo());
        cout << "\nPrzy opuszczaniu zasiegu wykonuja sie destruktory." << endl;
    }
    {
        cout << "\nI wszystko na raz." << endl;
        Int i;
        int j = i = foo(7);
    }
    {
        cout << "\nTworzymy ulamki 2/3 i 5/7." << endl;
        Rational a(2, 3), b(5, 7);
        cout << "\nMnozymy je." << endl;
        a *= b;
        cout << "\nA teraz znow mnozymy, ale wynik w nowym obiekcie." << endl;
        Rational c = a * b;
        cout << "\nOperator przypisania dla ulamkow." << endl;
        a = c;
        cout << "\nI znow destrukcja." << endl;
    }
}
