#include <iostream>

using namespace std;
typedef int v;
typedef int r;
#define mid ((range_begin + range_end) / 2)
#define leaf (range_begin == range_end)
#define base (begin == range_begin and end == range_end)
#define max(a, b) (a > b ? a : b)
#define min(a, b) (a < b ? a : b)

class IntervalTree {
    v M;
    IntervalTree *left, *right;
public:

    IntervalTree(r range_begin, r range_end) : M(0) {
        if (!leaf) {
            left = new IntervalTree(range_begin, mid);
            right = new IntervalTree(mid + 1, range_end);
        }
    }

    v maks(r begin, r end, r range_begin, r range_end) {
        if (M >= 0) {
            return M;
        }
        if (base) {
            return -M;
        }
        if (begin > mid) {
            return right->maks(begin, end, mid + 1, range_end);
        }
        if (end <= mid) {
            return left->maks(begin, end, range_begin, mid);
        }
        return max(left->maks(begin, mid, range_begin, mid), right->maks(mid + 1, end, mid + 1, range_end));
    }

    void set(r begin, r end, r range_begin, r range_end, v val) {
        if (base) {
            M = max(val, M);
        } else {
            if (M < 0) {
                M = min(-val, M);
            } else {
                left->M = M;
                right->M = M;
            }
            if (begin > mid) {
                right->set(begin, end, mid + 1, range_end, val);
            } else if (end <= mid) {
                left->set(begin, end, range_begin, mid, val);
            } else {
                left->set(begin, mid, range_begin, mid, val);
                right->set(mid + 1, end, mid + 1, range_end, val);
            }
            if (left->M >= 0 and left->M == right->M) {
                M = left->M;
            } else {
                M = -max(abs(left->M), abs(right->M));
            }
        }

    }

    /*void print_tree() {
        cout << "Node [" << range_begin << ".." << range_end << "] - M: " << M << ", m: " << m << "\n";
        if (!isLeaf()) {
            left->print_tree();
            right->print_tree();
        }

    }*/
};

int main() {
    int d, n;
    cin >> d >> n;
    IntervalTree tree(0, d);
    for (int i = 0; i < n; ++i) {
        int l, x;
        cin >> l >> x;
        auto h = tree.maks(x, x + l - 1, 0, d);
        //cout << "max on [" << x << ".." << x + l - 1 << "] is " << h << "\n";
        tree.set(x, x + l - 1, 0, d, h + 1);
        //tree.print_tree();
    }
    cout << tree.maks(0, d, 0, d);
    return 0;
}
