#include <iostream>
#include <vector>
#include <queue>
#include <set>

bool vis[500000];
bool car[500000];
using std::cout;
using std::cin;
using std::vector;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    int n, m;
    cin >> n >> m;
    vector<vector<int>> v(n);
    for (int i = 0; i < n; ++i) {
        cin >> car[i];
    }
    std::set<int> udane;
    for (int i = 0; i < m; ++i) {
        int a, b;
        cin >> a >> b;
        --a; --b;
        v[a].push_back(b);
        v[b].push_back(a);
    }
    std::queue<int> q;
    q.push(0);
    vis[0] = true;
    while (!q.empty()) {
        int a = q.front();
        q.pop();
        for (auto& i : v[a]) {
            if (car[i]) {
                udane.insert(i);
            } else if (!vis[i]) {
                vis[i] = true;
                q.push(i);
            }
        }
    }
    for (auto& i : udane) {
        cout << i + 1 << "\n";
    }

    return 0;
}