#include <iostream>
#include <vector>

using namespace std;
int main() {
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n;
    vector<int> products(n, 0);
    for (int i = n - 1; i >= 0; --i) {
        cin >> products[i];
    }

    int m;
    cin >> m;
    vector<long long> res(n + 1, -2);
    long long sum = 0;
    int odd = -1;
    int even = -1;

    for (int i = 0; i < n; ++i) {
        sum += products[i];
        if (products[i] & 1) {
            odd = i;
        } else {
            even = i;
        }
        long long tempSum = sum;
        if (!(tempSum & 1)) {
            int newOdd = -1, newEven = -1;
            for (int j = i + 1; j < n; ++j) {
                if (newOdd < 0 and products[j] & 1) {
                    newOdd = j;
                } else if (newEven < 0 and !(products[j] & 1)) {
                    newEven = j;
                }
                if (newOdd != -1 and newEven != -1) {
                    if (odd == -1 ) {
                        tempSum += products[newOdd] - products[even];
                    } else if (even == -1) {
                        tempSum += products[newEven] - products[odd];
                    } else {
                        tempSum = max(tempSum - products[odd] + products[newEven], tempSum - products[even] + products[newOdd]);
                    }
                    break;
                }
            }
            if (newEven == -1 and newOdd == -1) {
                tempSum = -1;
            } else if (newEven == -1) {
                if (even == -1) {
                    tempSum = -1;
                } else {
                    tempSum += products[newOdd] - products[even];
                }
            } else if (newOdd == -1) {
                if (odd == -1) {
                    tempSum = -1;
                } else {
                    tempSum += products[newEven] - products[odd];
                }
            }
        }
        res[i + 1] = tempSum;
    }

    for (int i = 0; i < m; ++i) {
        int a;
        cin >> a;
        cout << res[a] << "\n";
    }
    return 0;
}