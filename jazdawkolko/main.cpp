#include <iostream>
#include <vector>
#include <queue>
#include <tuple>

bool vis[500000];
using std::cout;
using std::cin;
using std::vector;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    int n, m;
    cin >> n >> m;
    if (m < 3) {
        cout << "NIE";
        return 0;
    }
    vector<vector<int>> v(n);
    for (int i = 0; i < m; ++i) {
        int a, b;
        cin >> a >> b;
        --a; --b;
        v[a].push_back(b);
        v[b].push_back(a);
    }

    std::queue<std::pair<int, int>> q;
    for (int i = 0; i < n; ++i) {
        if (vis[i]) continue;
        q.emplace(i, -1);
        vis[i] = true;
        while (!q.empty()) {
            int u, prev;
            std::tie(u, prev) = q.front();
            q.pop();
            for (auto& i : v[u]) {
                if (!vis[i]) {
                    vis[i] = true;
                    q.emplace(i, u);
                } else if (i != prev) {
                    cout << "TAK"; return 0;
                }
            }
        }
    }

    cout << "NIE";
    return 0;
}
/*
200000 3



*/