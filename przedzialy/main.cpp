#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>

using std::cout;
using std::cin;
using std::vector;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    unsigned int n;
    cin >> n;
    vector<std::pair<int, bool>> v(n * 2);
    for (unsigned int i = 0; i < n; ++i) {
        int a, b;
        cin >> a >> b;
        v[2 * i] = std::make_pair(a, true);
        v[2 * i + 1] = std::make_pair(b, false);
    }
    std::sort(v.begin(), v.end());
    int last_end = -1;
    bool in_interval = false;
    int minimum = std::numeric_limits<int>::max();
    for (auto& i : v) {
        if (i.second) {
            if (in_interval) {
                cout << 0;
                return 0;
            }
            minimum = std::min(minimum, i.first - last_end);
        } else {
            last_end =  i.first;
        }
        in_interval = i.second;
    }
    cout << minimum;
    return 0;
}