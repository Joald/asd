#include <iostream>
#include <vector>
#define LL long long
using std::cin;
using std::cout;
void first() {
    cout << ">\n";
}

void second() {
    cout << "<\n";
}
void equal() {
    cout << "=\n";
}

//typedef long long LL;
const LL q = 1000000000;
LL modulo(LL a, LL mod = q) {
    LL r = a % mod;
    return r >= 0 ? r : r + std::abs(mod);
}
std::vector<LL> P;
std::vector<LL> S;
std::vector<LL> B;

LL hash(LL i, LL j) {
    if (i == 0) return P[j];
    return modulo(P[j] - modulo(P[i - 1] * B[j - i + 1]));
}

int main() {
    freopen("in.txt", "r", stdin);
    unsigned LL n, m;
    cin >> n >> m;
    P.resize(n);
    B.resize(n);
    S.resize(n);
    for (auto& i : S) {
        char c;
        cin >> c;
        i = int(c - 'a') + 1;
    }
    LL bb = 91;
    B[0] = 91;
    for (int i = 1; i < B.size(); ++i) {
        B[i] = modulo(B[i - 1] * bb);
    }
    LL H = 0;
    for (int i = 0; i < n; ++i) {
        H = modulo(H * bb);
        H = modulo(H + S[i]);
        P[i] = H;
    }
    for (int i = 0; i < m; ++i) {
        int a, b, c, d;
        cin >> a >> b >> c >> d;
        a--;b--;c--;d--;
        LL len1 = b - a + 1;
        LL len2 = d - c + 1;
        LL l = std::min(len1, len2);
        LL h1 = hash(a, a + l);
        LL h2 = hash(c, c + l);
        if (h1 > h2) {
            first();
        } else if (h2 > h1) {
            second();
        } else {
            if (len1 > len2) {
                first();
            } else if (len2 > len1) {
                second();
            } else {
                equal();
            }
        }

    }
    return 0;
}