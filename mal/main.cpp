#include <iostream>

using namespace std;
typedef int v;
typedef int r;
#define mid midd(range_begin, range_end)
#define leaf isLeaf(range_begin, range_end)
#define base thisIsIt(range_begin, range_end, begin, end)
#define size sizef(range_begin, range_end)
#define lsize sizef(range_begin, mid)
#define rsize sizef(mid + 1, range_end)
#define color (type == WHITE ? 1 : 0)
enum Type {
    WHITE, BLACK, UNKNOWN
};
string tab[3] = {"WHITE", "BLACK", "UNKNOWN"};
class IntervalTree {
    v number;
    Type type;
    IntervalTree *left, *right;

public:
    int midd(r begin, r end) {
        return (begin + end) / 2;
    }

    IntervalTree(r range_begin, r range_end) : number(0), type(BLACK) {
        if (!leaf) {
            left = new IntervalTree(range_begin, mid);
            right = new IntervalTree(mid + 1, range_end);
        }
    }

    bool isLeaf(r begin, r end) {
        return end == begin;
    }

    bool thisIsIt(r begin, r end, r range_begin, r range_end) {
        return begin == range_begin and end == range_end;
    }

    /*v maks(r begin, r end, r range_begin, r range_end) {
        if (thisIsIt(begin, end, range_begin, range_end)) {
            return max(m, M);
        }
        if (begin > mid) {
            return max(M, right->maks(begin, end, mid + 1, range_end));
        }
        if (end <= mid) {
            return max(M, left->maks(begin, end, range_begin, mid));
        }
        return max(M, max(left->maks(begin, mid, range_begin, mid), right->maks(mid + 1, end, mid + 1, range_end)));
    }*/

    int sizef(r range_begin, r range_end) {
        return range_end - range_begin + 1;
    }

    void updateSons(r range_begin, r range_end) {
        left->type = type;
        right->type = type;
        left->number = color * lsize;
        right->number = color * rsize;
        type = UNKNOWN;
    }

    void updateColor(r range_begin, r range_end) {
        if (number == size) {
            type = WHITE;
        } else if (number == 0) {
            type = BLACK;
        }
    }

    int set(r begin, r end, r range_begin, r range_end, Type val) {
        if (base) {
            type = val;
            number = size * color;
        } else {
            if (type != UNKNOWN) {
                updateSons(range_begin, range_end);
            }
            if (begin > mid) {
                number = left->number + right->set(begin, end, mid + 1, range_end, val);
            } else if (end <= mid) {
                number = right->number + left->set(begin, end, range_begin, mid, val);
            } else {
                number = 0;
                number += left->set(begin, mid, range_begin, mid, val);
                number += right->set(mid + 1, end, mid + 1, range_end, val);
            }
            updateColor(range_begin, range_end);
        }
        return number;
    }

    void print_tree(r range_begin, r range_end) {
        cout << "Node [" << range_begin << ".." << range_end << "] - number: " << number << ", type: " << tab[type] << "\n";
        if (!leaf) {
            left->print_tree(range_begin, mid);
            right->print_tree(mid + 1, range_end);
        }

    }
    /*
 12 4 1 5 C 2 10 B 4 6 B 4 7 C
 */
};

int main() {
    int n, m;
    cin >> n >> m;
    IntervalTree tree(1, n);
    for (int i = 0; i < m; ++i) {
        int a, b;
        char c;
        cin >> a >> b >> c;
        cout << tree.set(a, b, 1, n, c == 'B' ? WHITE : BLACK) << "\n";
        //tree.print_tree(1, n);
    }
    return 0;
}