#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>

#define mid ((first + last) / 2)
#define isLeaf (first == last)
#define contains(x) (first <= (x) && (x) <= last)
#define isOnLeft(x) (first <= (x) && (x) <= mid)
#define isOnRight(x) (mid < (x) && (x) <= last)
#define isExact (rleft == first && rright == last)
using I = int;
using itt = std::vector<I>::iterator;
class IntervalTree {
    I first, last;
    IntervalTree *left, *right;//, *parent;
    bool clear;
    I sum;//, fake_value, fake_place;
    IntervalTree(I first, I last) :
            first(first), last(last),
            left(first >= last ? nullptr : new IntervalTree(first, mid)),
            right(first >= last ? nullptr : new IntervalTree(mid + 1, last)),
            //parent(parent),
            clear(false), sum(0)
            //, fake_value(0), fake_place(-1)
    {assert(first <= last);}
    void updateSum() {
        sum = left->sum + right->sum;
    }
public:
    explicit IntervalTree(I n) : IntervalTree(1, n) {}
    void start(std::vector<int>& v, int begin, int end) {
        if (isLeaf) {
            sum = v[begin];
            return;
        }
        auto midd = (end + begin) / 2;
        left->start(v, begin, midd);
        right->start(v, midd + 1, end);
        updateSum();
    }
    int remove(int rleft, int rright) {
        if (rleft > rright) return 0;
        if (isExact) {
            if (!isLeaf) {
                clear = true;
            }
            int deleted = sum;
            sum = 0;
            return deleted;
        }
        if (clear) {
            return 0;
        }
        int toReturn;
        if (rleft > mid) {
            toReturn = right->remove(rleft, rright);
        } else if (rright <= mid) {
            toReturn = left->remove(rleft, rright);
        } else {
            toReturn = left->remove(rleft, mid) + right->remove(mid + 1, rright);
        }
        updateSum();
        return toReturn;
    }/*
    int move(int rleft, int rright, int dest) {
        if (rleft > rright || (rleft == rright && rright == dest)) return 0;
        int toReturn;
        if (isOnLeft(dest)) {
            if (isOnLeft(rright)) {
                toReturn = left->move(rleft, rright, dest);
            } else {
                toReturn = left->move(rleft, mid, dest) + right->remove(mid + 1, rright);
            }
        } else if (isOnRight(rleft)) {
            toReturn = right->move(rleft, rright, dest);
        } else {
            toReturn = left->remove(rleft, mid) + right->move(mid + 1, rright, dest);
        }
        updateSum();
        return toReturn;
    }*/
    void add(int where, int what) {
        if (isLeaf) {
            sum += what;
            return;
        }
        if (clear) {
            left->clear = true;
            left->sum = 0;
            right->clear = true;
            right->sum = 0;
            clear = false;
        }
        sum += what;
        if (isOnLeft(where)) {
            left->add(where, what);
        } else {
            right->add(where, what);
        }
    }
};

using std::cout;
using std::cin;
int main() {
    std::ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin.tie(nullptr);
    int n, q;
    cin >> n >> q;
    IntervalTree tree(n);
    std::vector<int> starter(static_cast<unsigned long>(n + 1));
    for (int i = 0; i < n; ++i) {
        int a;
        cin >> a;
        starter[a]++;
    }
    tree.start(starter, 1, n);
    for (int i = 0; i < q; ++i) {
        int a, b, c;
        cin >> a >> b >> c;
        int x;
        if (a == b) {
            x = 0;
        } else if (c == a) {
            x = tree.remove(a + 1, b);
        } else if (c == b) {
            x = tree.remove(a, b - 1);
        } else {
            x = tree.remove(a, c - 1) + tree.remove(c + 1, b);
        }

        if (x) tree.add(c, x);
        cout << x << "\n";
    }
    return 0;
}
/*
5 3
1 2 2 4 5
3 5 3
1 1 1
1 2 2
*/

/*
50 22
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
16 16 16
26 26 26
4 4 4
25 27 26
15 16 16
38 43 43
18 21 19
7 12 7
16 18 18
10 18 11
11 15 11
15 22 18
26 35 33
33 39 38
37 41 41
24 31 24
29 37 35
24 36 36
13 18 16
30 34 32
17 25 24
4 8 5
37 40 37
22 35 34

*/
